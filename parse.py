#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
#
# imports
import argparse, sys
#
# FUNCTIONS
#
def arguments():
    # Parse arguments
    parser = argparse.ArgumentParser(prog="minion", description="An automated website compiler using multimarkdown", epilog="minion (C)2015 Kirk Dawson")
    parser.add_argument('-v','--version', action='version', version='minion 2.0')
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-i", "--init", action="store_true", help="initialise minion in current directory")
    group.add_argument("-e", "--encode", action="store_true", help="batch encode images and move to includes")
    group.add_argument("-b", "--build", action="store_true", help="build current minion site")
    group.add_argument("-s", "--serve", action="store_true", help="serve current build on port:1313")
    group.add_argument("-p", "--publish", action="store_true", help="publish current build")
    args = parser.parse_args()
    # test for no argument
    if (args.build is False and args.encode is False and args.init is False and args.publish is False and args.serve is False):
        print "usage: minion [-h] [-v] [-i | -e | -b | -s | -p]"
        print "minion: error: unrecognized arguments: "
        sys.exit (1)
    # determine process to br performed
    if args.init:
        action = 'init'
    if args.encode:
        action = 'encode'
    if args.build:
        action = 'build'
    if args.serve:
        action = 'serve'
    if args.publish:
        action = 'publish'
    return action
