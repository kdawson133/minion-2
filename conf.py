#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
# Imports

#
# Functions
#
def create_conf():
    print "--> pub.conf file created Target: ~/Temp/"
    print
    lines = ['[Target]\n', 'directory: ~/Temp/\n']
    conf = open("pub.conf", "w+")
    conf.writelines(lines)
    conf.close()
    return
#
