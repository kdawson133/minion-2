#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
# Imports
import os, shutil
#
# Functions
#
def clean_minion():
    # clean previous build if present
    print "cleaning previous builds..."
    print "--> CLEAN"
    print
    if os.path.exists("out/"):
        shutil.rmtree("out/", ignore_errors=True)
    return
