#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
# Copyright (c) 2015 Kirk Dawson. This program is open-source software,
# and may be redistributed under the terms of the MIT license. See the
# LICENSE file in this distribution for details.
#
# minion is a simple static site generator
# that utilises multimarkdown to render
# HTML pages. Version 2.0
#
#
# imports
import dependency, parse, process
#
# FUNCTIONS
#
def main():
    dependency.check()
    mode = parse.arguments()
    process.action(mode)




if __name__ == "__main__":
    main()
