#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
#
# imports
import os
from set_color import OKBLUE, ENDC, FAIL
from conf import create_conf
#
# FUNCTIONS
#
def init_minion():
    # initialise minion and build structure
    print OKBLUE + "Initialsing minion..." + ENDC
    print
    if not os.path.exists("content"):
        os.makedirs("content")
        print "--> content directory created for MultiMarkdown files"
        print
    if not os.path.exists("media"):
        os.makedirs("media")
        print "--> media directory created for Image files"
        print
    if not os.path.exists("encodes"):
        os.makedirs("encodes")
        print "--> encodes directory created for images to be encoded"
        print
    if not os.path.exists("includes"):
        os.makedirs("includes")
        print "--> includes directory created for included Text files"
        print
    if not os.path.exists("public"):
        os.makedirs("public")
        print "--> public directory created for CSS, fonts and .js files"
        print
    if not os.path.exists("pub.conf"):
        create_conf()
    print OKBLUE + "Initialisation Complete" + ENDC
    print
    return
#
