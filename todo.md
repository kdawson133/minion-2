# minion todo list

* Optimise the build module.
    * STATUS: IN PROGRESS


* Incorporate native python rsync replacement.
    * see [dirsync](https://pypi.python.org/pypi/dirsync/2.1)
    * STATUS: IN PROGRESS


* Incorporate native python CSS compressor.
    * see [minify](https://pypi.python.org/pypi/minify/)
    * STATUS: IN PROGRESS


* Incorporate native python JS compressor.
    * see [minify](https://pypi.python.org/pypi/minify/)
    * STATUS: IN PROGRESS


* Incorporate native python http-server replacement.
    * see [http-server](https://docs.python.org/2/library/simplehttpserver.html)
    * STATUS: IN PROGRESS


* Investigate native python alternatives for MultiMarkDown.
    * STATUS: Still researching...


* Add all possible Markdown file extensions???
    * `.md`, `.mmd` & `.markdown`
    * STATUS: IN PROGRESS
    * **Remember markdown is a subset of multimarkdown**.

* Add to initialise module to create blank `pub.conf`.
    * STATUS: COMPLETE
