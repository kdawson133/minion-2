#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
#
# imports
import timeit, os, base64, sys
from set_color import OKBLUE, OKGREEN, FAIL, ENDC
#
# FUNCTIONS
#
def encode_minion():
    # start timing
    betic = timeit.default_timer()
    # set current directory
    cur_dir = os.getcwd()
    # Check if minion has been intialised
    if not os.path.exists("content"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        sys.exit(2)
    if not os.path.exists("includes"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        sys.exit(2)
    if not os.path.exists("media"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        sys.exit(2)
    if not os.path.exists("public"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        sys.exit(2)
    if not os.path.exists("encodes"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        sys.exit(2)
    print ""
    print OKBLUE + "minion is batch encoding images..." + ENDC
    print ""
    # Check if there is any images to encode
    print "searching for images to encode..."
    pngimgcount = 0
    jpgimgcount = 0
    gifimgcount = 0
    for root, directories, filenames in os.walk("encodes/"):
        for filename in filenames:
            walkfilename = os.path.join(root,filename)
            if walkfilename.endswith('.png'):
                pngimgcount = pngimgcount + 1
            if walkfilename.endswith('.jpg'):
                jpgimgcount = jpgimgcount + 1
            if walkfilename.endswith('.gif'):
                gifimgcount = gifimgcount + 1
    imgcount = pngimgcount + jpgimgcount + gifimgcount
    print "-->", imgcount, " image files found"
    print ""
    if imgcount == 0:
        print FAIL + "no images to encode!" + ENDC
        print ""
        sys.exit(2)
    elif imgcount != 0:
        for root, directories, filenames in os.walk("encodes/"):
            for filename in filenames:
                walkfilename = os.path.join(root,filename)
                if (walkfilename.endswith('.png') or walkfilename.endswith('.gif') or walkfilename.endswith('.jpg')):
                    full_path = os.path.abspath(walkfilename)
                    extension = os.path.splitext(full_path)[1]
                    basename = os.path.basename(full_path)
                    new_full_path = cur_dir + '/includes/' + basename + '.txt'
                    # get optional caption
                    print OKGREEN + basename + ENDC
                    caption = raw_input('enter caption (enter for none): ')
                    # output for user
                    print "encoding..."
                    print
                    print "--> encoding: " + full_path
                    print "--------> to: " + new_full_path
                    print
                    # open the image file
                    fin = open(full_path, 'rb')
                    # read the data from the image file
                    bin_data = fin.read()
                    # close the image file
                    fin.close()
                    # convert image data from binary to base 64
                    b64_data = base64.b64encode(bin_data)
                    # convert to markdown format
                    md_data = "![" + caption + "](data:image/" + extension + ";base64," + b64_data + ")"
                    # write data to file
                    fout = open(new_full_path, 'w')
                    fout.write(md_data)
                    fout.close()
                    os.remove(full_path)
    betoc = timeit.default_timer()
    bett = betoc - betic
    print ""
    print OKBLUE + "batch encode complete" + ENDC
    print ("time taken: %.3f seconds" % bett)
    print ""
    return
