#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
#
import timeit, os, sys
from set_color import OKBLUE, OKGREEN, FAIL, ENDC
#
def serve_minion():
    stic = timeit.default_timer()
    print OKBLUE + "stand By..." + ENDC
    if not os.path.exists("out/"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be built!"
        print ""
        sys.exit(2)
    sso = "http-server out -p 1313"
    os.system(sso)
    stoc = timeit.default_timer()
    stt = stoc - stic
    print ""
    stimemins = stt / 60
    print ("server uptime: %.3f minutes" % stimemins)
    return
