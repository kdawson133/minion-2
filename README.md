# MINION README

This README details the steps necessary to get minion up and running.

### What is this repository for?


* minion is static site generator using mulimarkdown.
* Version 2.0


### How do I get set up?


Download the executable <a href="https://bitbucket.org/kdawson133/minion-2/src/7f8b773a6e02afe6277658cc0e899e95bba4c2eb/dist/minion?at=master">here</a>.</p></li>
Install - copy the executable `minion` to your path.</p></li>
Configuration - a file named `pub.conf` will be created in your local directory. Edit this file specifying your target directory(directory to publish to) as follows:</p></li>

```
[Target]
directory: /path/to/your/webserver/directory
```

### Dependencies:


* [yuicompressor](https://github.com/yui/yuicompressor/releases)
* [multimarkdown](http://fletcherpenney.net/multimarkdown/download)
* [http-server](https://www.npmjs.com/package/http-server)
* [rsync](https://rsync.samba.org/download.html)

### Documentation:

* `minion --init` creates the directory structure required for your project in the current directory and creates the following directories:
    * **content** - where your markdown files are stored. categories are created with sub-directories.
    * **media** - where your images, videos and other files are stored. this directory and sub-directories are copied as is to your site when published. linking to them via relative links is recommended.
    * **includes** - where your included text files are stored. these files are compiled into the resulting HTML files and will remain here.
    * **encodes** - where you put your image files you wish to convert to base64 encoded text files. when encoded they are moved to your includes directory.
    * **public** - where you put your CSS, JS and font files. Any CSS and JS files will be compressed on build and moved to the public directory in your site. The font files are moved as is to the public directory in your site.
    * `pub.conf` file is created with the target directory of ~/Temp. This needs to be edited to reflect where you want your site to be published.

* `minion --encode` encodes any JPG, PNG and GIF files to base64 TXT file and moved to the includes directory. As each image is converted you are are asked for a caption for the image.

* `minion --build` builds your site. The resulting website will be stored in the out directory.
    * markdown is converted to HTML incorporating any includes.
    * The pubic and media directories are copied to the out directory.
    * CSS and JS are compress and stored alongside the originals.

* `minion --serve` this will start a web server on port **1313** so you can check your site prior to publishing.

* `minion --publish` uploads you site to the directory specified in the `pub.conf` file in your local directory as specified above in the configuration section.

### Who do I Talk to?

[Kirk Dawson](https://bitbucket.org/kdawson133)
