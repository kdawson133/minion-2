#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
# Imports
import os
from set_color import OKBLUE, OKGREEN, FAIL, ENDC, WARNING
#
# Functions
#
def minify_js():
    #Look for JS
    print "searching for JS..."
    listing = os.listdir("public")
    jscount = 0
    for files in listing:
        if (files.endswith('.js') and (not files.endswith('.min.js'))):
            jscount = jscount + 1
    if jscount > 0:
        print "-->", jscount, "JS files found"
    if jscount == 0:
        print WARNING + "WARNING: " + ENDC + "no JS found"
    print
    # Convert minified js
    if jscount >= 1:
        print "converting js to min.jss..."
        pathtooutpublic = os.path.dirname(os.path.realpath("out/public/"))
        for root, directories, filenames in os.walk("out/public/"):
            for filename in filenames:
                walkfilename = os.path.join(root,filename)
                if (walkfilename.endswith('.js' ) and (not walkfilename.endswith('.min.js' ))):
                    newext = ".min.js"
                    jsminfilename = os.path.splitext(walkfilename)[0] + newext
                    print "--> Compressing: " + pathtooutpublic + "/" + walkfilename
                    print "-----------> To: " + pathtooutpublic + "/" + jsminfilename
                    yuicmd = "yuicompressor " + walkfilename + " > " + jsminfilename
                    os.system(yuicmd)
                    # os.remove(walkfilename)
                    print

    return
#
