#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
# Imports
import os, shutil, sys
from set_color import OKBLUE, OKGREEN, FAIL, ENDC, WARNING
#
# Functions
#
def markdown_convert():
    # look for markdown content
    print "searching for content..."
    pathtominion = os.getcwd()
    mdcount = 0
    for root, directories, filenames in os.walk("content/"):
        for filename in filenames:
            walkfilename = os.path.join(root,filename)
            if walkfilename.endswith('.md'):
                mdcount = mdcount + 1
            if walkfilename.endswith('.mmd'):
                mdcount = mdcount + 1
            if walkfilename.endswith('.markdown'):
                mdcount = mdcount + 1
    print "-->", mdcount, " markdown files found"
    print
    if mdcount == 0:
        print FAIL + "no content to build!" + ENDC
        print
        sys.exit(2)
    # Convert markdown to html
    print "converting markdown to HTML..."
    # Looking for markdown
    for root, directories, filenames in os.walk("content/"):
        for filename in filenames:
            walkfilename = os.path.join(root,filename)
            if (walkfilename.endswith('.md') or walkfilename.endswith('.mmd') or walkfilename.endswith('.markdown')):
                in_rel_path = os.path.relpath(walkfilename)
                out_rel_path = in_rel_path.replace("content/", "out/", 1)
                in_htmlfilename = os.path.splitext(in_rel_path)[0] + ".html"
                out_htmlfilename = os.path.splitext(out_rel_path)[0] + ".html"
                print "--> converting: " + pathtominion + "/" + walkfilename
                print "---> moving to: " + pathtominion + "/" + out_htmlfilename
                print
                bms = "multimarkdown -o --batch --to=html --process-html --smart --notes "
                mmd_out = "--output=\"" + out_htmlfilename + "\" "
                fms = bms + mmd_out + walkfilename
                os.system(fms)

    return
#
