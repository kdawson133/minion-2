#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
#
# imports
import timeit, os, shutil, sys
from set_color import OKBLUE, OKGREEN, FAIL, ENDC, WARNING
from clean import clean_minion
from md import markdown_convert
from export import export_minion
from css import minify_css
from js import minify_js
#
# FUNCTIONS
#
def build_minion():
    # Start timing
    tic = timeit.default_timer()
    # Check if minion has been intialised
    if (not os.path.exists("content") or not os.path.exists("includes") or not os.path.exists("media") or not os.path.exists("public") or not os.path.exists("encodes")):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print
        sys.exit(2)
    # Starting Build
    print
    print OKBLUE + "minion is commencing build..." + ENDC
    print
    # clean previous build if present
    clean_minion()
    # Exporting Content
    export_minion()
    # look for markdown content and Convert
    markdown_convert()
    # Look for CSS and minify
    minify_css()
    # Look for JS and minify
    minify_js()
    toc = timeit.default_timer()
    tt = toc - tic
    print
    print OKBLUE + "build complete" + ENDC
    print ("time taken: %.3f seconds" % tt)
    print

    return
#
