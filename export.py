#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
# Imports
import shutil, os, time
from set_color import OKBLUE, OKGREEN, FAIL, ENDC, WARNING
#
# Functions
#
def export_minion():
    print "exporting directory structure ..."
    shutil.copytree("content/","out/", ignore = shutil.ignore_patterns('*.*'))
    print "--> EXPORTED"
    print
    
    print "exporting public files..."
    shutil.copytree("public/","out/public/", ignore = shutil.ignore_patterns('.*'))
    print "--> EXPORTED"
    print

    print "exporting media files ..."
    shutil.copytree("media/","out/media/", ignore = shutil.ignore_patterns('.*'))
    print "--> EXPORTED"
    print

    return
#
