#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
#
global HEADER
HEADER = '\033[95m'
global OKBLUE
OKBLUE = '\033[34m'
global OKGREEN
OKGREEN = '\033[32m'
global WARNING
WARNING = '\033[35m'
global FAIL
FAIL = '\033[31m'
global ENDC
ENDC = '\033[0m'
global BOLD
BOLD = '\033[1m'
global UNDERLINE
UNDERLINE = '\033[4m'
