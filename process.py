#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
#
# imports
from init import init_minion
from encode import encode_minion
from build import build_minion
from serve import serve_minion
from publish import publish_minion
#
# FUNCTIONS
#
def action(mode):
    if mode == 'init':
        init_minion()
    elif mode == 'encode':
        encode_minion()
    elif mode == 'build':
        build_minion()
    elif mode == 'serve':
        serve_minion()
    elif mode == 'publish':
        publish_minion()

    return
