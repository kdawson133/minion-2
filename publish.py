#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
# Imports
import os, timeit, ConfigParser, sys
from set_color import OKBLUE, OKGREEN, FAIL, ENDC
#
# Functions
#
def publish_minion():
    ptic = timeit.default_timer()
    print OKBLUE + "publishing ..." + ENDC
    print
    # check if minion site has been built
    if not (os.path.exists("out") and os.path.exists("out/index.html")):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be built"
        print
        sys.exit(2)
    # check for config file
    if not os.path.exists("pub.conf"):
        print FAIL + "ERROR: " + ENDC + "pub.conf file not found!"
        print
        sys.exit(2)
    config = ConfigParser.ConfigParser()
    config.read('pub.conf')
    target = config.get('Target','directory')
    expand_target = os.path.expanduser(target)
    print "target: " + expand_target
    # check if target directory exists
    if not os.path.exists(expand_target):
        print FAIL + "ERROR: " + ENDC + "target diectory does not exist!"
        print
        sys.exit(2)
    # rsync the output diectory with the target directory
    rsync_cmd = "rsync -avzhW --exclude '.*' --delete" + " out/ " + expand_target
    os.system(rsync_cmd)
    ptoc = timeit.default_timer()
    ptt = ptoc - ptic
    print
    print OKBLUE + "publishing complete" + ENDC
    print ("time taken: %.3f seconds" % ptt)
    print

    return
#
