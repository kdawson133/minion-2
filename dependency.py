#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
#
# imports
import os, sys
from set_color import OKBLUE, ENDC, FAIL
#
# FUNCTIONS
#
def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None
#
def check():
    # Check if dependacies have been installed
    dep_1 = 0
    dep_2 = 0
    dep_3 = 0
    dep_4 = 0

    if which("yuicompressor") is None:
        print FAIL + "ERROR: " + ENDC + "Dependency yuicompressor not installed!"
        print "See https://github.com/yui/yuicompressor/releases"
        dep_1 = 1
    if which("multimarkdown") is None:
        print FAIL + "ERROR: " + ENDC + "Dependency multimarkdown not installed!"
        print "See http://fletcherpenney.net/multimarkdown/download/"
        dep_2 = 1
    if which("http-server") is None:
        print FAIL + "ERROR: " + ENDC + "Dependency http-server not installed!"
        print "See https://www.npmjs.com/package/http-server"
        dep_3 == 1
    if which("rsync") is None:
        print FAIL + "ERROR: " + ENDC + "Dependency rsync not installed!"
        print "See https://rsync.samba.org/"
        dep_4 = 1
    if (dep_1 or dep_2 or dep_3 or dep_4) == 1:
        sys.exit(2)
    return
