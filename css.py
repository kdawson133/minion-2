#!/usr/bin/env python
#-*- coding: utf-8 -*-
#
#
# Imports
import os
from set_color import OKBLUE, OKGREEN, FAIL, ENDC, WARNING
#
# Functions
#
def minify_css():
    #Look for CSS
    print "Searching for CSS..."
    listing = os.listdir("public")
    csscount = 0
    for files in listing:
        if (files.endswith('.css' ) and (not files.endswith('.min.css' ))):
            csscount = csscount + 1
    if csscount > 0:
        print "-->", csscount, "CSS files found"
    if csscount == 0:
        print WARNING + "WARNING: " + ENDC + "no CSS found"
    print ""
    # Convert minified css
    if csscount >= 1:
        print "Converting css to min.css..."
        pathtooutpublic = os.path.dirname(os.path.realpath("out/public/"))
        for root, directories, filenames in os.walk("out/public/"):
            for filename in filenames:
                walkfilename = os.path.join(root,filename)
                if (walkfilename.endswith('.css' ) and (not walkfilename.endswith('.min.css' ))):
                    newext = ".min.css"
                    cssminfilename = os.path.splitext(walkfilename)[0] + newext
                    print "--> Compressing: " + pathtooutpublic + "/" + walkfilename
                    print "-----------> To: " + pathtooutpublic + "/" + cssminfilename
                    yuicmd = "yuicompressor " + walkfilename + " > " + cssminfilename
                    os.system(yuicmd)
                    # os.remove(walkfilename)
                    print ""


    return
#
